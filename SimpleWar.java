public class SimpleWar{
	public static void main(String[] args){
		Deck deck = new Deck();
		deck.shuffle();
		System.out.println(deck);
		
		int pointsP1 = 0;
		int pointsP2 = 0;
		
		System.out.println("Player 1 score: " + pointsP1);
		System.out.println("Player 2 score: " + pointsP2);
			
		while(deck.length()>0){
			Card cardP1 = deck.drawTopCard();
			System.out.println(cardP1);
			
			Card cardP2 = deck.drawTopCard();
			System.out.println(cardP2);
			
			if(cardP1.calculateScore()<cardP2.calculateScore()){
				pointsP1++;
			}
			else{
				pointsP2++;
			}
			
			System.out.println("Player 1 score: " + pointsP1);
			System.out.println("Player 2 score: " + pointsP2);
		}
		
		if(pointsP1>pointsP2){
			System.out.println("Player 1 wins. Congrats!");
		}
		else if(pointsP1<pointsP2){
			System.out.println("Player 2 wins. Congrats!");
		}
		else{
			System.out.println("It's a tie");
		}
	}
}