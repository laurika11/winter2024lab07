import java.util.Random;

public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		this.rng = new Random();
		this.numberOfCards = 52;
		this.cards = new Card[numberOfCards];
		String[] suits = {"Spades", "Hearts", "Clubs", "Diamond"};
		String[] values = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"};
		
		int countSuits = 0;
		int countValues = 0;
		for(int i=0; i<cards.length; i++){
			if(countSuits < 3){
				cards[i] = new Card(suits[countSuits], values[countValues]);
				countSuits++;
			}else{
				cards[i] = new Card(suits[countSuits], values[countValues]);
				countSuits = 0; //this reset the suits to start at 0 again
			}
			if(countValues < 12){
				cards[i] = new Card(suits[countSuits], values[countValues]);
				countValues++;
			}else{
				cards[i] = new Card(suits[countSuits], values[countValues]);
				countValues = 0; //this reset the values to start at 0 again
			}
		}
	}
	
	public int length(){
		return this.numberOfCards; //this return the number of cards in the deck
	}
	
	public Card drawTopCard(){
		this.numberOfCards--; //this substract one card from the deck
		return this.cards[this.numberOfCards]; //returns the card in the last position of the array
	}
	
	public String toString(){
		String result = "";
		for(int i=0; i<numberOfCards; i++){
			result = result + this.cards[i].toString() + "\n";
		}
		return result; //returns the cards in the Card array as a string
	}
	
	public void shuffle(){
		for(int i=0; i<numberOfCards; i++){
			int random = rng.nextInt(numberOfCards);
			Card temporaryCard = this.cards[i];
			this.cards[i] = this.cards[random];
			this.cards[random] = temporaryCard;
		} //shuffle the cards
	}
}