public class Card{
	private String suit;
	private String value;
	
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public String getValue(){
		return this.value;
	}
	
	public String toString(){
		return value + " of " + suit;
	}
	
	public double calculateScore(){
		double score = Integer.parseInt(this.value);
		//double score = rank
		
		if(this.suit.equals("Hearts")){
			score += 0.4;
		}
		else if(this.suit.equals("Diamonds")){
			score += 0.3;
		}
		else if(this.suit.equals("Clubs")){
			score += 0.2;
		}
		else{
			score += 0.1;
		}
		return score;
	}
}